import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent {
  constructor(private snackBar: MatSnackBar) {}
  showSpinner = false;

  loadData(): void {
    this.showSpinner = true;

    setTimeout(() => {
      this.showSpinner = false;
      this.openSnackBar('hello');
    }, 4000);
  }
  openSnackBar(message: string): void {
    this.snackBar.open(message);
  }
}
